import "./App.css";
import "./config/colors.css";
import { Route, HashRouter as Router, Switch } from "react-router-dom";
import { connect } from "react-redux";
import CompleteAccount from "./pages/complete-account/CompleteAccount";
import SignIn from "./pages/sign-in/SignIn";
import Navbar from "./components/Navbar/Navbar";
import assets from "./assets/assets.json";
import Home from "./pages/home/Home";
import QuestionBox from "./components/Questions/QuestionBox/QuestionBox";
import CreateForm from "./pages/create-form/CreateForm";
import ResponseForm from "./pages/response-form/ResponseForm";

const handleColorVariable = () => {
  try {
    //console.log(colors[0]);
    var root = document.querySelector(":root");

    root.style.setProperty("--main-color", assets[0].mainColor);
    root.style.setProperty("--second-color", assets[0].secondColor);
    root.style.setProperty("--third-color", assets[0].thirdColor);
    root.style.setProperty("--nav-btn-current", assets[0].navBtnCurrent);
    root.style.setProperty("--nav-btn-hover", assets[0].navBtnHover);
  } catch (error) {
    console.log(error);
  }
};

function App(props) {
  console.log("logged? " + props.loggedIn);

  handleColorVariable();

  const logo_src = assets[0].logo;

  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/question" render={() => <QuestionBox />} />
          <Route path="/complete" exact component={CompleteAccount} />
          <Route path="/create" exact component={CreateForm} />
          <Route path="/form/:formId" exact component={ResponseForm} />
          <Route
            path="/"
            exact
            render={() =>
              props.loggedIn ? <Home logo={logo_src} /> : <SignIn />
            }
          />
        </Switch>
      </Router>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    loggedIn: state.loggedIn,
  };
};

export default connect(mapStateToProps)(App);
