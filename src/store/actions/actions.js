import * as actionTypes from "./actionTypes";

export const authenticate = (username, password) => {
  //console.log(username);
  //console.log(password);
  return {
    type: actionTypes.AUTHENTICATE,
    user: {
      username,
      password,
    },
  };
};
