import * as actionTypes from "../actions/actionTypes";
import * as loginUtils from "../../utils/loginUtils";

let user = JSON.parse(localStorage.getItem("user"));

const initialState = user
  ? { authToken: "123", loggedIn: true, user }
  : { authToken: "", loggedIn: false };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHENTICATE:
      return loginUtils.login(state, action.user);

    default:
      return state;
  }
};

export default reducer;
