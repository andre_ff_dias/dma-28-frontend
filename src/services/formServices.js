import API from "../config/api";

export default {
  /*getForm(formId){
        return API.get("").then()
    }*/
  getMenus() {
    return API.get("menus/").then((res) => {
      return res.data;
    });
  },
  createMenu(data) {
    return API.post("menus/", data).then((res) => {
      return res.data;
    });
  },
  getTags() {
    return API.get("tags/").then((res) => {
      return res.data;
    });
  },
  createForm(data) {
    return API.post("forms/", data).then((res) => {
      return res.data;
    });
  },
  getForm(formId) {
    return API.get("forms/" + formId + "/").then((res) => {
      //console.log(res);
      return res.data;
    });
  },
  answerForm(data) {
    return API.post("formanswer/", data).then((res) => {
      console.log(res);
      return res.data;
    });
  },
};
