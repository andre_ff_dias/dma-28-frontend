import i18n from "i18next";
import { initReactI18next } from "react-i18next";
// detect user language
import LanguageDetector from "i18next-browser-languagedetector";

// import translation files
import translationEN from "../translations/en/general.json";
import translationPT from "../translations/pt/general.json";

const resources = {
  en: {
    translation: translationEN,
  },
  pt: {
    translation: translationPT,
  },
};

i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources,
    fallbackLng: "en", // in case there isn't the detected language. The default language will be English
    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
  });

export default i18n;
