import { Button, makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import QuestionBox from "../../components/Questions/QuestionBox/QuestionBox";
import formServices from "../../services/formServices";
import { useTranslation } from "react-i18next";
import "./style.css";

const useStyles = makeStyles(() => ({
  submitBtn: {
    marginBottom: 10,
    minWidth: 80,
    textTransform: "none",
    color: "#fff",
    background: "#57AF41",
    "&:hover": {
      backgroundColor: "#57AF41",
    },
  },
}));

function ResponseForm(props) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("form-defs", {
    returnObjects: true,
  });

  const [form, setForm] = useState([]);
  const [formFields, setFormFields] = useState([]);
  const [answers, setAnswers] = useState([]);

  useEffect(() => {
    const formId = props.match.params.formId;
    if (formId !== undefined) {
      formServices.getForm(formId).then((res) => {
        //console.log(res);
        setForm(res);
        setFormFields(res.fields);

        const aux = [];
        res.fields.forEach((field) => {
          aux.push({ field: field.id, answer: "" });
        });

        setAnswers(aux);
      });
    }
  }, [props.match.params.formId]);

  const handleAnswer = (newAnswer, id) => {
    const tempAnswers = [...answers];
    const i = tempAnswers.findIndex((aux) => aux.field === id);
    if (i || i === 0) {
      tempAnswers[i].answer = newAnswer;
    }

    setAnswers(tempAnswers);
  };

  const handleSaveAnswer = () => {
    const data = {
      form: form.id,
      tags: form.tags,
      user: 1,
      answers: answers,
    };

    formServices.answerForm(data).then((res) => {
      console.log(res);
    });
    console.log(JSON.stringify(data));
  };

  return (
    <div className="response-form">
      <div className="form-title">{form.name}</div>
      <div className="questions">
        {formFields.map((field, i) => (
          <div key={i}>
            <QuestionBox
              id={field.id}
              editable={false}
              question={field}
              handleAnswer={handleAnswer}
            />
          </div>
        ))}
      </div>
      <div className="form-answer-btns">
        <Button
          className={classes.submitBtn}
          onClick={handleSaveAnswer}
          variant="contained"
        >
          {translated.saveBtn}
        </Button>
      </div>
    </div>
  );
}

export default ResponseForm;
