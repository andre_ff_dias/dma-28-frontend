import React from "react";
import "./style.css";
import { useTranslation } from "react-i18next";

function Home(props) {
  const { t } = useTranslation();

  // Return all text of home-page as an Object from the translation files
  const translated = t("home-page", { returnObjects: true });

  return (
    <div className="home-page">
      <img src={props.logo} alt="Logo" />
      <br />
      <p className="message">{translated.paragraph}</p>
    </div>
  );
}

export default Home;
