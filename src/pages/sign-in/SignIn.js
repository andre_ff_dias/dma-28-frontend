import React, { useState, createRef } from "react";
import { useTranslation } from "react-i18next";
import {
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  makeStyles,
  OutlinedInput,
  TextField,
} from "@material-ui/core";
import "./style.css";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { connect } from "react-redux";

import * as actions from "../../store/actions/actions";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      margin: 10,
      width: 300,
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
    "& .MuiButton-root": {
      margin: "10px auto",
    },
    "& .MuiCheckbox-root": {
      padding: 0,
    },
  },
  button: {
    textTransform: "none",
  },
}));

function SignIn(props) {
  const classes = useStyles();

  const { t } = useTranslation();

  // Return all text of sign-in-page as an Object from the translation files
  const translated = t("sign-in-page", { returnObjects: true });

  const [values, setValues] = useState({
    email: "",
    password: "",
    showPassword: false,
  });

  const username = createRef("");
  const password = createRef("");

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const signIn = (e) => {
    e.preventDefault();
    //console.log("Email: " + values.email);
    props.onLoginClick(values.email, values.password);
  };

  return (
    <div className="sign-in">
      <div className="form-box">
        <h1 className="header">{translated.header}</h1>
        <form className={classes.root} autoComplete="off" onSubmit={signIn}>
          <TextField
            id="email-input"
            required
            type="email"
            value={values.email}
            onChange={handleChange("email")}
            label={translated.email}
            variant="outlined"
            ref={username}
          />
          <FormControl required variant="outlined">
            <InputLabel htmlFor="password-input">
              {translated.password}
            </InputLabel>
            <OutlinedInput
              id="password-input"
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              ref={password}
              onChange={handleChange("password")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
              labelWidth={80}
            />
          </FormControl>
          <br />
          <Button type="submit" variant="contained" color="primary">
            {translated.button}
          </Button>
        </form>
        <div className="new-account">{translated.newAccont}</div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLoginClick: (username, password) =>
      dispatch(actions.authenticate(username, password)),
  };
};

export default connect(null, mapDispatchToProps)(SignIn);
