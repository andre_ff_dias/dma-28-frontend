import { IconButton, makeStyles } from "@material-ui/core";
import React, { useState } from "react";
import FormDefs from "../../components/form-defs/FormDefs";
import QuestionBox from "../../components/Questions/QuestionBox/QuestionBox";
import { MdAdd } from "react-icons/md";
import "./style.css";
import formServices from "../../services/formServices";

const useStyles = makeStyles(() => ({
  addbtn: {
    "& .MuiButtonBase-root": {
      borderRadius: 50,
      background: "#E1E1E1",
      marginTop: -30,
      marginBottom: 15,
    },
  },
}));

function CreateForm() {
  const classes = useStyles();

  const [fields, setFields] = useState([
    {
      field: "",
      required: false,
      sensitive: false,
      searchable: false,
      type: {
        description: "",
      },
      options: [],
    },
  ]);
  const [formInfo, setFormInfo] = useState({
    name: "",
    description: "",
    menu: { description: "" },
    state: {
      description: "Published",
    },
    searchable_type: { description: "None" },
    tags: [],
    rgpd_description: null,
    add_answers: true,
    edit_answers: false,
    delete_answers: false,
    retention: null,
  });

  const handleFieldChanges = (changeType, newData, i) => {
    let fieldsTemp = [...fields];

    switch (changeType) {
      case "title":
        fieldsTemp[i].field = newData;
        break;
      case "required":
        fieldsTemp[i].required = newData;
        break;
      case "searchable":
        fieldsTemp[i].searchable = newData;
        break;
      case "sensitive":
        fieldsTemp[i].sensitive = newData;
        break;
      case "options":
        fieldsTemp[i].options = newData;
        break;
      case "type":
        fieldsTemp[i].type = newData;
        break;
      default:
        break;
    }
    //console.log(fieldsTemp);
    setFields(fieldsTemp);
  };

  const handleFormChanges = (changeType, newData) => {
    let formInfoTemp = formInfo;

    switch (changeType) {
      case "formTitle":
        formInfoTemp.name = newData;
        break;
      case "description":
        formInfoTemp.description = newData;
        break;
      case "menu":
        formInfoTemp.menu.description = newData;
        break;

      default:
        break;
    }
    //console.log(formInfoTemp);
    setFormInfo(formInfoTemp);
  };

  const handleSave = () => {
    let data = {
      name: formInfo.name,
      description: formInfo.description,
      menu: formInfo.menu,
      tags: formInfo.tags,
      searchable_type: formInfo.searchable_type,
      rgpd_description: formInfo.rgpd_description,
      add_answers: formInfo.add_answers,
      edit_answers: formInfo.edit_answers,
      delete_answers: formInfo.delete_answers,
      retention: formInfo.retention,
      state: formInfo.state,
      fields,
    };
    if (formInfo.name !== "") {
      formServices.createForm(data).then((res) => {
        console.log(res);
      });
      //console.log(JSON.stringify(data));
    } else {
      console.log("Error");
    }
  };

  const hadleAddField = () => {
    setFields((fields) => [
      ...fields,
      {
        field: "",
        required: false,
        sensitive: false,
        searchable: false,
        type: {
          id: "",
          description: "",
        },
        options: [],
      },
    ]);
  };

  const handleDeleteField = (id) => {
    let fieldsTemp = [...fields];
    if (fieldsTemp.length > 1) {
      fieldsTemp.splice(id, 1);
    }
    setFields(fieldsTemp);
  };

  const handleDuplicateField = (id) => {
    let fieldsTemp = [...fields];
    let newField = {
      field: fieldsTemp[id].field,
      required: fieldsTemp[id].required,
      sensitive: fieldsTemp[id].sensitive,
      searchable: fieldsTemp[id].searchable,
      type: {
        id: fieldsTemp[id].type.id,
        description: fieldsTemp[id].type.description,
      },
      options: [],
    };
    setFields((fields) => [...fields, newField]);
  };

  return (
    <div className="create-form">
      <FormDefs handleFormChanges={handleFormChanges} handleSave={handleSave} />
      <div className="questions">
        {fields.map((field, num) => (
          <div key={num}>
            <QuestionBox
              editable={true}
              id={num}
              handleDeleteField={handleDeleteField}
              handleDuplicateField={handleDuplicateField}
              handleFieldChanges={handleFieldChanges}
              question={field}
            />
          </div>
        ))}
        <div className={classes.addbtn}>
          <IconButton onClick={hadleAddField}>
            <MdAdd />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default CreateForm;
