import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Button,
  Checkbox,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  makeStyles,
  OutlinedInput,
  TextField,
} from "@material-ui/core";
import "./style.css";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { Link } from "react-router-dom";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      margin: 7,
      width: 300,
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
    "& .MuiButton-root": {
      margin: "10px auto",
    },
    "& .MuiCheckbox-root": {
      padding: 0,
    },
  },
}));

function CompleteAccount() {
  const classes = useStyles();

  const { t } = useTranslation();

  // Return all text of complete-account-page as an Object from the translation files
  const translated = t("complete-account-page", { returnObjects: true });

  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    passwordConfirm: "",
    checkedTerms: false,
    showPassword: false,
    showPasswordConfirm: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleChangeCheckBox = (event) => {
    setValues({ ...values, [event.target.name]: event.target.checked });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClickShowPasswordConfirm = () => {
    setValues({ ...values, showPasswordConfirm: !values.showPasswordConfirm });
  };

  const complete = (e) => {
    e.preventDefault();
    if (values.checkedTerms) {
      console.log("Name: " + values.name + "; Email: " + values.email);
    } else {
      console.log("Terms not checked!");
    }
  };

  return (
    <div className="complete-account">
      <div className="form-box">
        <h1 className="header">{translated.header}</h1>
        <form className={classes.root} autoComplete="off" onSubmit={complete}>
          <TextField
            id="name-input"
            required
            type="text"
            value={values.name}
            onChange={handleChange("name")}
            label={translated.name}
            variant="outlined"
          />
          <TextField
            id="email-input"
            required
            type="email"
            value={values.email}
            onChange={handleChange("email")}
            label={translated.email}
            variant="outlined"
          />
          <FormControl required variant="outlined">
            <InputLabel htmlFor="password-input">
              {translated.password}
            </InputLabel>
            <OutlinedInput
              id="password-input"
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              onChange={handleChange("password")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
              labelWidth={80}
            />
          </FormControl>
          <FormControl required variant="outlined">
            <InputLabel htmlFor="password-confirm-input">
              {translated.passwordConfirm}
            </InputLabel>
            <OutlinedInput
              id="password-confirm-input"
              type={values.showPasswordConfirm ? "text" : "password"}
              value={values.passwordConfirm}
              onChange={handleChange("passwordConfirm")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPasswordConfirm}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPasswordConfirm ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              }
              labelWidth={150}
            />
          </FormControl>
          <br />
          <div className="form-terms">
            <Checkbox
              checked={values.checkedTerms}
              onChange={handleChangeCheckBox}
              name="checkedTerms"
            />
            <span>{translated.terms}</span>
          </div>
          <Button type="submit" variant="contained" color="primary">
            {translated.button}
          </Button>
        </form>
        <Link className="login-redirect" to="/login">
          {translated.loginLink}
        </Link>
      </div>
    </div>
  );
}

export default CompleteAccount;
