import React, { useState } from "react";
import { IconContext } from "react-icons/lib";
import { FaBars } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";
import "./Style.css";
import { Link } from "react-router-dom";
import styled from "styled-components";
import MenuData from "./MenuData";
import { HiOutlineTranslate } from "react-icons/hi";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useTranslation } from "react-i18next";
import Flag from "react-world-flags";
import i18n from "i18next";

const SidebarMenu = styled.nav`
  background: #fff;
  width: 256px;
  height: 100vh;
  display: flex;
  justify-content: center;
  position: fixed;
  top: 0;
  left: ${({ sidebar }) => (sidebar ? "0" : "-100%")};
  transition: 350ms;
  z-index: 10;
`;

export default function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const [open, setOpen] = useState(false);
  const { t } = useTranslation();

  // Return all text of navbar as an Object from the translation files
  const translated = t("navbar", { returnObjects: true });

  const showSidebar = () => setSidebar(!sidebar);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLng = (lng) => {
    i18n.changeLanguage(lng);
    handleClose();
  };

  return (
    <>
      <IconContext.Provider value={{ color: "#fff" }}>
        <div className="nav">
          <div className="nav-left">
            <Link className="nav-icon" to="#">
              <FaBars onClick={showSidebar} />
            </Link>
          </div>
          <div className="nav-right">
            <HiOutlineTranslate
              className="nav-btn-language"
              onClick={handleClickOpen}
            />
          </div>
        </div>
      </IconContext.Provider>
      <IconContext.Provider value={{ color: "#15171c" }}>
        <SidebarMenu sidebar={sidebar}>
          <div className="sidebar-wrap">
            <Link className="nav-icon-close" to="#">
              <AiOutlineClose onClick={showSidebar} />
            </Link>
            <MenuData />
          </div>
        </SidebarMenu>
      </IconContext.Provider>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>{translated.language}</DialogTitle>
        <DialogContent className="flags">
          <Flag
            className="flag-btn"
            code="PT"
            height="50"
            onClick={() => handleLng("pt")}
          />
          <br />
          <br />
          <Flag
            className="flag-btn"
            code="gb"
            height="50"
            width="75"
            onClick={() => handleLng("en")}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
