import React from "react";
import { useTranslation } from "react-i18next";
import { AiFillHome } from "react-icons/ai";
import { IoIosCreate } from "react-icons/io";
import { Link, useLocation } from "react-router-dom";

export default function MenuData() {
  const { t } = useTranslation();

  // Return all text of navbar as an Object from the translation files
  const translated = t("navbar", { returnObjects: true });

  const location = useLocation();
  //console.log(location.pathname);

  return (
    <>
      <Link
        className={`sidebar-btn ${location.pathname === "/" ? " current" : ""}`}
        to="/"
      >
        <AiFillHome className="sidebar-btn-icon" />
        {translated.data.home}
      </Link>
      <Link
        className={`sidebar-btn ${
          location.pathname === "/create" ? " current" : ""
        }`}
        to="/create"
      >
        <IoIosCreate className="sidebar-btn-icon" />
        {translated.data.create}
      </Link>
    </>
  );
}
