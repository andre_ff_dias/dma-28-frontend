import {
  FormControl,
  FormControlLabel,
  FormLabel,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import React from "react";
import { useTranslation } from "react-i18next";
import "./Style.css";

const useStyles = makeStyles(() => ({
  radioGroup: {
    marginLeft: "10%",
    display: "flex",
    "& .MuiFormLabel-root": {
      marginTop: 10,
      marginBottom: 5,
      fontSize: "1.2rem",
      display: "flex",
    },
  },
}));

function Boolean({ title, handleAnswerChange }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  return (
    <div className="boolean">
      <FormControl component="fieldset" className={classes.radioGroup}>
        <FormLabel>{title}</FormLabel>
        <RadioGroup name="boolean-options" onChange={handleAnswerChange}>
          <FormControlLabel
            value="yes"
            control={<Radio />}
            label={translated.booleanTrue}
          />
          <FormControlLabel
            value="no"
            control={<Radio />}
            label={translated.booleanFalse}
          />
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export default Boolean;
