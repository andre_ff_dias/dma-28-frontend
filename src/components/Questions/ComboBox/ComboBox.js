import React, { useState } from "react";
import {
  Button,
  Checkbox,
  FormControlLabel,
  IconButton,
  TextField,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { MdAdd, MdClose } from "react-icons/md";
import "./Style.css";

function ComboBox({ handleOptionsChange }) {
  const [numberOption, setNumberOption] = useState(1);

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  const [options, setOptions] = useState([{ text: translated.option }]);

  const handleOptionValue = (event, i) => {
    var optionsTemp = [...options];
    optionsTemp[i].text = event.target.value;
    setOptions(optionsTemp);
    handleOptionsChange(options);
  };

  const addOption = () => {
    setNumberOption(numberOption + 1);
    var optionsTemp = options;
    //console.log(numberOption);
    optionsTemp.push({ text: translated.option });
    setOptions(optionsTemp);
    handleOptionsChange(options);
  };

  const removeOption = (i) => {
    setNumberOption(numberOption - 1);
    var optionsTemp = options;
    if (optionsTemp.length > 1) {
      optionsTemp.splice(i, 1);
    }
    setOptions(optionsTemp);
    handleOptionsChange(options);
  };

  return (
    <div className="combo-box">
      <div className="options">
        {options.map((opt, i) => (
          <div key={i} className="option">
            <div className="option-input">
              <Checkbox disabled />
              <TextField
                placeholder={translated.option}
                value={opt.text}
                onChange={(event) => {
                  handleOptionValue(event, i);
                }}
              />
            </div>
            <div className="option-delete">
              {numberOption > 1 ? (
                <IconButton
                  aria-label="delete"
                  onClick={() => {
                    removeOption(i);
                  }}
                >
                  <MdClose />
                </IconButton>
              ) : null}
            </div>
          </div>
        ))}
        <div className="add-option">
          <FormControlLabel
            disabled
            control={<MdAdd />}
            label={
              <Button
                onClick={() => {
                  addOption();
                }}
              >
                {translated.addOption}
              </Button>
            }
          />
        </div>
      </div>
    </div>
  );
}

export default ComboBox;
