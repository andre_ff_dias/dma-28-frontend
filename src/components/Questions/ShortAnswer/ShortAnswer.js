import React, { useState } from "react";
import { makeStyles, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import "./Style.css";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 10,
      marginBottom: 15,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
}));

function ShortAnswer({ title, required, handleAnswerChange }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("error-msgs", {
    returnObjects: true,
  });

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const handleChange = (event) => {
    if (required && event.target.value === "") {
      setError(true);
      setErrorMsg(translated.required);
    }
    handleAnswerChange(event);
  };

  //console.log(required);
  return (
    <div className="short-answer">
      <div className={classes.root}>
        <TextField
          error={error}
          helperText={error ? errorMsg : " "}
          required={required}
          label={title}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}

export default ShortAnswer;
