import React from "react";
import {
  FormControlLabel,
  makeStyles,
  Switch as SwitchBtn,
  TextField,
  IconButton,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { MdDelete } from "react-icons/md";
import { HiDocumentDuplicate } from "react-icons/hi";
import ComboBox from "../ComboBox/ComboBox";
//import SelectForm from "../Select/Select";
import Boolean from "../Boolean/Boolean";
import ShortAnswer from "../ShortAnswer/ShortAnswer";
import LongAnswer from "../LongAnswer/LongAnswer";
import Email from "../Email/Email";
import Date from "../Date/Date";
import Switch, { Case, Default } from "react-switch-case";
import "./Style.css";
import Url from "../Url/Url";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 10,
      marginBottom: 15,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
  selectControl: {
    position: "absolute",
    left: 30,
    bottom: 0,
    minWidth: 150,
  },
}));

function QuestionBox({
  id,
  editable,
  handleDeleteField,
  handleDuplicateField,
  handleFieldChanges,
  handleFieldOptionsChanges,
  handleAnswer,
  question,
}) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  const handleRequiredChange = () => {
    handleFieldChanges("required", !question.required, id);
  };

  const handleSearchableChange = () => {
    handleFieldChanges("searchable", !question.searchable, id);
  };

  const handleSensitiveChange = () => {
    handleFieldChanges("sensitive", !question.sensitive, id);
  };

  const handleTitleChange = (event) => {
    handleFieldChanges("title", event.target.value, id);
  };

  const handleOptionsChange = (options) => {
    handleFieldChanges("options", options, id);
  };

  const handleAnswerChange = (event) => {
    handleAnswer(event.target.value, id);
  };

  const handleQuestionTypeChange = (event) => {
    switch (event.target.value) {
      case 1:
        handleFieldChanges("type", { id: 1, description: "Text Short" }, id);
        break;
      case 2:
        handleFieldChanges("type", { id: 2, description: "Text Long" }, id);
        break;
      case 4:
        handleFieldChanges("type", { id: 4, description: "ComboBox" }, id);
        break;
      /*case "select":
        handleFieldChanges("type", { id: 6, description: "Check Group" }, id);
        break;*/
      case 7:
        handleFieldChanges("type", { id: 7, description: "Email" }, id);
        break;
      case 8:
        handleFieldChanges("type", { id: 8, description: "URL" }, id);
        break;
      case 10:
        handleFieldChanges("type", { id: 10, description: "Date" }, id);
        break;
      case 13:
        handleFieldChanges("type", { id: 13, description: "Boolean" }, id);
        break;
      default:
        handleFieldChanges("type", { id: 0, description: "" }, id);
        break;
    }
  };

  const handleDelete = () => {
    handleDeleteField(id);
  };

  const handleDuplicate = () => {
    handleDuplicateField(id);
  };

  return (
    <>
      {editable ? (
        <div className="question-box">
          <div className="switch-btns">
            <FormControlLabel
              control={
                <SwitchBtn
                  checked={question.required}
                  onChange={handleRequiredChange}
                  name="required"
                  color="primary"
                />
              }
              label={translated.required}
              labelPlacement="start"
            />
            <FormControlLabel
              control={
                <SwitchBtn
                  checked={question.searchable}
                  onChange={handleSearchableChange}
                  name="required"
                  color="primary"
                />
              }
              label={translated.searchable}
              labelPlacement="start"
            />
            <FormControlLabel
              control={
                <SwitchBtn
                  checked={question.sensitive}
                  onChange={handleSensitiveChange}
                  name="required"
                  color="primary"
                />
              }
              label={translated.sensitive}
              labelPlacement="start"
            />
          </div>
          <div className={classes.root}>
            <TextField
              id="title-input"
              required
              type="text"
              onChange={handleTitleChange}
              label={translated.title}
              value={question.field}
              variant="outlined"
            />
          </div>
          <div>
            <Switch condition={question.type.id}>
              <Case value={4}>
                <ComboBox handleOptionsChange={handleOptionsChange} />
              </Case>
              <Default>{null}</Default>
            </Switch>
          </div>
          <div className="footer-box">
            <FormControl className={classes.selectControl}>
              <InputLabel id="select-type-label">Question Type</InputLabel>
              <Select
                labelId="select-type-label"
                id="select-type"
                onChange={handleQuestionTypeChange}
                value={question.type.id}
              >
                <MenuItem value={1}>
                  {translated.selectMenu.shortAnswer}
                </MenuItem>
                <MenuItem value={2}>
                  {translated.selectMenu.longAnswer}
                </MenuItem>
                <MenuItem value={4}>{translated.selectMenu.comboBox}</MenuItem>
                <MenuItem value={7}>{translated.selectMenu.email}</MenuItem>
                <MenuItem value={8}>URL</MenuItem>
                <MenuItem value={10}>{translated.selectMenu.date}</MenuItem>
                <MenuItem value={13}>{translated.selectMenu.boolean}</MenuItem>
              </Select>
            </FormControl>
            <div className="footer-btns">
              <IconButton onClick={handleDuplicate}>
                <HiDocumentDuplicate />
              </IconButton>
              <IconButton onClick={handleDelete}>
                <MdDelete />
              </IconButton>
            </div>
          </div>
        </div>
      ) : (
        <div className="question-box">
          <Switch condition={question.type.id}>
            <Case value={1}>
              <ShortAnswer
                required={question.required}
                title={question.field}
                handleAnswerChange={handleAnswerChange}
              />
            </Case>
            <Case value={2}>
              <LongAnswer
                required={question.required}
                title={question.field}
                handleAnswerChange={handleAnswerChange}
              />
            </Case>
            <Case value={4}>
              <ComboBox
                required={question.required}
                title={question.field}
                handleOptionsChange={handleOptionsChange}
              />
            </Case>
            <Case value={7}>
              <Email
                required={question.required}
                title={question.field}
                handleAnswerChange={handleAnswerChange}
              />
            </Case>
            <Case value={8}>
              <Url
                required={question.required}
                title={question.field}
                handleAnswerChange={handleAnswerChange}
              />
            </Case>
            <Case value={10}>
              <Date required={question.required} title={question.field} />
            </Case>
            <Case value={13}>
              <Boolean
                required={question.required}
                title={question.field}
                handleAnswerChange={handleAnswerChange}
              />
            </Case>
            <Default>{null}</Default>
          </Switch>
        </div>
      )}
    </>
  );
}

export default QuestionBox;
