import React, { useState } from "react";
import { makeStyles, TextField } from "@material-ui/core";
import { validUrl } from "../../../config/regex";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 10,
      marginBottom: 15,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
}));

export default function Url({ title, required, handleAnswerChange }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("error-msgs", {
    returnObjects: true,
  });

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const handleChange = (event) => {
    if (event.target.value !== "") {
      const valid = validUrl.test(event.target.value);
      setError(!valid);
      if (valid) {
        handleAnswerChange(event);
      } else {
        setErrorMsg(translated.url);
      }
    } else if (required) {
      setError(true);
      setErrorMsg(translated.required);
    }
  };

  return (
    <div className="url">
      <div className={classes.root}>
        <TextField
          error={error}
          helperText={error ? errorMsg : " "}
          id="url"
          label={title}
          required={required}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}
