import React from "react";
import {
  InputAdornment,
  makeStyles,
  TextField
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { IoMdCalendar } from "react-icons/io";
import "./Style.css";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiInputBase-root": {
      marginBottom: 15,
    },
  },
}));

function Date({ editable }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  return (
    <div className="date">
        <div className={classes.root}>
          <TextField
            type="date"
            defaultValue="2021-05-19"
            label={translated.date}
            id="date-disabled"
            InputLabelProps={{
              shrink: true,
            }}            
            InputProps={{
              endAdornment: 
                <InputAdornment position="end">
                  <IoMdCalendar />
                </InputAdornment>
            }}    
          />
        </div>
    </div>
  );
}

export default Date;
