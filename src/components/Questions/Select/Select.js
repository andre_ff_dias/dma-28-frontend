import React, { useState } from "react";
import {
  Button,
  FormControlLabel,
  InputAdornment,
  TextField,
  IconButton,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { MdAdd, MdClose } from "react-icons/md";
import "./Style.css";

function SelectForm() {
  const [numberOption, setNumberOption] = useState(1);

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  const [options, setOptions] = useState([{ text: translated.option }]);

  const handleOptionValue = (event, i) => {
    const optionsTemp = options;
    optionsTemp[i].text = event.target.value;
    setOptions([optionsTemp]);
  };

  const addOption = () => {
    const optionsTemp = options;
    setNumberOption(numberOption + 1);
    optionsTemp.push({ text: translated.option });
  };

  const removeOption = (i) => {
    setNumberOption(numberOption - 1);
    var optionsTemp = options;
    if (optionsTemp.length > 1) {
      optionsTemp.splice(i, 1);
    }
    setOptions(optionsTemp);
  };

  return (
    <div className="select">
      <div className="options">
        {options.map((opt, i) => (
          <div key={i} className="option">
            <div className="option-input">
              <TextField
                placeholder={translated.option}
                value={opt.text}
                onChange={(event) => {
                  handleOptionValue(event, i);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">{i + 1}.</InputAdornment>
                  ),
                }}
              />
            </div>
            <div className="option-delete">
              {numberOption > 1 ? (
                <IconButton
                  aria-label="delete"
                  onClick={() => {
                    removeOption(i);
                  }}
                >
                  <MdClose />
                </IconButton>
              ) : null}
            </div>
          </div>
        ))}
        <div className="add-option">
          <FormControlLabel
            disabled
            control={<MdAdd />}
            label={
              <Button
                size="small"
                onClick={() => {
                  addOption();
                }}
              >
                {translated.addOption}
              </Button>
            }
          />
        </div>
      </div>
    </div>
  );
}

export default SelectForm;
