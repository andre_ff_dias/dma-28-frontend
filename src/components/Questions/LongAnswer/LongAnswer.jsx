import React from "react";
import {
  makeStyles,
  TextField
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import "./Style.css";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 10,
      marginBottom: 15,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
}));

function LongAnswer({ editable, handleAnswerChange, required  }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("questions", {
    returnObjects: true,
  });

  return (
    <div className="long-answer">
        <div className={classes.root}>
          <TextField
          required={required}
            multiline
            rows={2}
            id="longAnser-disabled"
            defaultValue={translated.longAnswer}
            onChange={handleAnswerChange}
          />
        </div>
    </div>
  );
}

export default LongAnswer;
