import React, { useState } from "react";
import { makeStyles, TextField } from "@material-ui/core";
import "./Style.css";
import { validEmail } from "../../../config/regex";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 10,
      marginBottom: 15,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
}));

function Email({ title, required, handleAnswerChange }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("error-msgs", {
    returnObjects: true,
  });

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const handleChange = (event) => {
    if (event.target.value !== "") {
      const valid = validEmail.test(event.target.value);
      setError(!valid);
      if (valid) {
        handleAnswerChange(event);
      } else {
        setErrorMsg(translated.email);
      }
    } else if (required) {
      setError(true);
      setErrorMsg(translated.required);
    }
  };

  return (
    <div className="email">
      <div className={classes.root}>
        <TextField
          error={error}
          helperText={error ? errorMsg : " "}
          id="email"
          label={title}
          required={required}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}

export default Email;
