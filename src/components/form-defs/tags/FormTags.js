import { Chip, TextField } from "@material-ui/core";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import React, { Component } from "react";
import API from "../../../config/api";

export default class FormTags extends Component {
  state = {
    tags: [],
  };

  componentDidMount() {
    API.get("tags/").then((res) => {
      console.log(res.data);
      const tags = res.data;
      this.setState({ tags });
    });
  }
  render() {
    return (
      <Autocomplete
        multiple
        id="tags-filled"
        options={this.state.tags.map((tag) => tag.description)}
        freeSolo
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              label={option}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant="standard"
            label="Tags"
            placeholder="Form Tags"
          />
        )}
        style={{ width: "80%", marginLeft: "10%" }}
      />
    );
  }
}
