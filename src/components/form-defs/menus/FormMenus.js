import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  FormControl,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import formService from "../../../services/formServices";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(() => ({
  selectControl: {
    minWidth: 150,
  },
}));

function FormMenus({ handleMenuChange }) {
  const classes = useStyles();

  const { t } = useTranslation();
  // Return all text of navbar as an Object from the translation files
  const translated = t("form-defs", { returnObjects: true });

  const [menus, setMenus] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentMenu, setCurrentMenu] = useState("");
  const [newMenu, setNewMenu] = useState("");

  useEffect(() => {
    formService.getMenus().then((data) => {
      //console.log(data);
      setMenus(data);
    });
  }, []);

  const handleChange = (event) => {
    const tempValue = event.target.value;
    if (tempValue === 0) {
      setOpen(true);
    } else {
      const menu = menus.find((aux) => aux.id === tempValue);
      setCurrentMenu(menu.id);
      //console.log(menu);
      handleMenuChange("menu", menu.description);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleNewMenuSave = () => {
    const data = {
      description: newMenu,
    };

    if (!menus.find((aux) => aux.id === newMenu)) {
      formService.createMenu(data).then((res) => {
        //console.log(res);
        setMenus([...menus, { id: res.id, description: res.description }]);
        setCurrentMenu(res.id);
        handleMenuChange("menu", res.description);
      });
    }
    handleClose();
  };

  const handleNewMenu = (event) => {
    //console.log(event.target.value);
    setNewMenu(event.target.value);
  };

  return (
    <>
      <FormControl className={classes.selectControl}>
        <InputLabel id="menu-select-label">Menu</InputLabel>
        <Select
          labelId="menu-select-label"
          id="menu-select"
          onChange={handleChange}
          value={currentMenu}
        >
          {menus.map((menu) => (
            <MenuItem value={menu.id}>{menu.description}</MenuItem>
          ))}
          <MenuItem value={0}>{translated.addMenu}</MenuItem>
        </Select>
      </FormControl>
      <Dialog open={open} onClose={handleClose}>
        <DialogContent>
          <DialogContentText>{translated.addMenu}</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="new-menu-input"
            label={translated.newMenu}
            type="text"
            fullWidth
            onChange={handleNewMenu}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            {translated.cancelBtn}
          </Button>
          <Button
            onClick={handleNewMenuSave}
            color="primary"
            disabled={newMenu === "" ? true : false}
          >
            {translated.saveBtn}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default FormMenus;
