import React, { useState } from "react";
import { Button, makeStyles, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import "./Style.css";
import FormMenus from "./menus/FormMenus";
import FormTags from "./tags/FormTags";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControl-root": {
      marginTop: 20,
      width: "80%",
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
  description: {
    "& .MuiTextField-root": {
      width: "80%",
      marginTop: 5,
      marginBottom: 15,
    },
    "& .MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
  saveBtn: {
    minWidth: 75,
    textTransform: "none",
    color: "#fff",
    background: "#57AF41",
    "&:hover": {
      backgroundColor: "#57AF41",
    },
  },
  cancelBtn: {
    minWidth: 75,
    textTransform: "none",
    color: "#fff",
    background: "#EA0000",
    "&:hover": {
      backgroundColor: "#EA0000",
    },
  },
  selectControl: {
    minWidth: 150,
  },
  tags: {
    width: "80%",
    marginLeft: "10%",
    marginBottom: 10,
    "& > * + *": {
      marginTop: 50,
    },
  },
}));

function FormDefs({ handleFormChanges, handleSave }) {
  const classes = useStyles();

  const { t } = useTranslation();
  const translated = t("form-defs", {
    returnObjects: true,
  });

  const [errorTitle, setError] = useState(false);
  const [title, setTitle] = useState("");

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
    handleFormChanges("formTitle", event.target.value);
  };

  const handleDescriptionChange = (event) => {
    handleFormChanges("description", event.target.value);
  };

  const handleSaveBtn = () => {
    if (title === "") {
      setError(true);
    } else {
      setError(false);
      handleSave();
    }
  };

  return (
    <div className="form-defs">
      <div className={classes.root}>
        <TextField
          error={errorTitle}
          helperText={errorTitle ? `${translated.titleRequired}` : " "}
          id="title-form-input"
          required
          type="text"
          onChange={handleTitleChange}
          label={translated.title}
          variant="outlined"
        />
      </div>
      <div className={classes.description}>
        <TextField
          id="description-input"
          label={translated.description}
          multiline
          rows={2}
          onChange={handleDescriptionChange}
          variant="outlined"
        />
      </div>
      <div>
        <FormMenus handleMenuChange={handleFormChanges} />
      </div>
      <div>
        <FormTags />
      </div>
      <div className="form-defs-btns">
        <Button className={classes.cancelBtn} variant="contained">
          {translated.cancelBtn}
        </Button>
        <Button
          className={classes.saveBtn}
          onClick={handleSaveBtn}
          variant="contained"
        >
          {translated.saveBtn}
        </Button>
      </div>
    </div>
  );
}

export default FormDefs;
